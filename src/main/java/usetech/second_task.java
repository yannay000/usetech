package usetech;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class second_task {  // для тех, у кого есть свой файл

    String file_razd=",";

    public second_task(){

    }
    public second_task(String FileName,Integer count,Integer max_number) throws IOException {

        ArrayList<String> list = new ArrayList<>();
        for (int i=0;i<=max_number;i++){
            list.add(String.valueOf(i));
        }
        String workDir = System.getProperty("user.dir");
        //System.out.println(System.getProperty("user.dir"));
        File file = new File(workDir,FileName);
        if (!file.exists()) {
            file.createNewFile();
        }
        BufferedWriter br = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
        br.write("");
        br.close();
        if (max_number<count){
            System.out.println("Количество чисел не может превыщать величину самого большого числа. Создан пустой файл.");
            return;
        }
        if (count==0){
            System.out.println("Количество чисел не может быть равно нулю. Создан пустой файл.");
            return;
        }
        br = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file,true)));
        int number=0;
        for (int i=1;i<=count;i++){
            try {
                number=(int) (Math.random()*(max_number+2-i));
                //System.out.println(list.get(max_number+1-i));
                //System.out.println(list.get(number));
                if (i!=count) {
                    br.write(list.get(number) + file_razd);
                }
                else {
                    br.write(list.get(number));
                }
                list.remove(number);
            }
            catch (IOException e) {
                e.printStackTrace();
            }

        }
        br.close();
    }
    public void read(String FileName, String sort) throws IOException {
        if ((sort!="max")&&(sort!="min")){
            System.out.println("Нужно ввести max или min. Других сортировок нет.");
            return;
        }
        String workDir = System.getProperty("user.dir");
        File file = new File(workDir,FileName);
        BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream(file)));
        String line=br.readLine();
        br.close();
        System.out.println("Набор из файла: "+line);
        if (String.valueOf(line)=="null") {
            return;
        }
        line = line + file_razd;
        ArrayList<Integer> list = new ArrayList<>();
        int count=0;
        int max_value=0;
        while (line.indexOf(file_razd)!=-1){
            Integer value=Integer.valueOf(line.substring(0,line.indexOf(file_razd)));
            if (value>max_value){
                max_value=value;
            }
            list.add(value);
            line=line.substring(line.indexOf(file_razd)+1);
            count++;
        }
        System.out.println("Отсортированный набор: "+line);
        if (sort=="max"){
            sort_max(list,max_value);
        }
        else{
            sort_min(list,max_value);
        }
    }
    private void sort_max(ArrayList<Integer> list,Integer max_value){
        for (int i=0;i<=max_value;i++){
            if (list.contains(i)){
                System.out.println(i);
            }
        }
    }
    private void sort_min(ArrayList<Integer> list,Integer max_value){
        for (int i=max_value;i>=0;i--){
            if (list.contains(i)){
                System.out.println(i);
            }
        }
    }
}
