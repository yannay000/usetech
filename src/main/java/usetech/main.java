package usetech;

import java.io.IOException;

public class main {

    private static final String FileName= "second_task.csv";
    private static final String FileNameRead= "second_task.csv";

    public static void main( String[] args ) throws IOException {

        //для тех, у кого свой файл
        //second_task task2=new second_task();
        // название файла,откуда берем числа; количество чисел; самое болльшое допустимое число
        second_task task2=new second_task(FileName,10,20);
        // название файла для чтения; вид сортировки(max или min)
        task2.read(FileNameRead,"min");

        //подсчет факториала
        third_task task3=new third_task(10);
    }
}
