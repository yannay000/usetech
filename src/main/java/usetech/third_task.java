package usetech;

public class third_task {

    public third_task(int fact){
        if (fact<=0) {
            System.out.println("Только положительные числа");
            return;
        }
        else{
            System.out.println("Факториал "+fact+" равен: "+fact(fact));
        }
    }

    public int fact(int n){
        if (n<=0) {
            System.out.println("Только положительные");
            return -1;
        }
        if (n==1) {
            return n;
        }
        else {
            return (n*fact(n-1));
        }
    }
}
